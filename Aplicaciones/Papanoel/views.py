from django.shortcuts import render,redirect
# Create your views here.
def index(request):
    return render(request, 'index.html')
def home(request):
    return render(request, 'home.html')
def elfo(request):
    return render(request, 'elfo.html')
def fotos(request):
    return render(request, 'fotos.html')
def mensaje(request):
    return render(request, 'mensaje.html')
def carta(request):
    return render(request, 'carta.html')
def inauguracion(request):
    return render(request, 'inauguracion.html')
def artico(request):
    return render(request, 'artico.html')
def aurora(request):
    return render(request, 'aurora.html')
def laponia(request):
    return render(request, 'laponia.html')
def mensajenoel(request):
    return render(request, 'mensajenoel.html')
def pueblo(request):
    return render(request, 'pueblo.html')
def reno(request):
    return render(request, 'reno.html')
def rovaniemi(request):
    return render(request, 'rovaniemi.html')
def video(request):
    return render(request, 'video.html')

from django.apps import AppConfig


class PapanoelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.Papanoel'

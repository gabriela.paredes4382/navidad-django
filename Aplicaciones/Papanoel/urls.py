from django.urls import path
from . import views

urlpatterns=[
    path('',views.index),
    path('home/',views.home, name='homes'),
    path('elfo/',views.elfo, name='elfos'),
    path('fotos/',views.fotos, name='foto'),
    path('mensaje/',views.mensaje, name='mensajes'),
    path('carta/',views.carta, name='cartas'),
    path('artico/',views.artico, name='articos'),
    path('aurora/',views.aurora, name='auroras'),
    path('laponia/',views.laponia, name='laponias'),
    path('mensajenoel/',views.mensajenoel, name='mensajesnoel'),
    path('pueblo/',views.pueblo, name='pueblos'),
    path('reno/',views.reno, name='renos'),
    path('rovaniemi/',views.rovaniemi, name='rovaniemis'),
    path('video/',views.video, name='videos'),
    path('inauguracion/',views.inauguracion, name='inauguraciones'),
]
